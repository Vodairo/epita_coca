import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by thomas on 16/06/15.
 */
public class Cluster
{
    private int XBegin;
    private int YBegin;
    private int XEnd;
    private int YEnd;

    public Cluster(int XBegin, int YBegin, int XEnd, int YEnd)
    {
        this.XBegin = XBegin;
        this.YBegin = YBegin;
        this.XEnd = XEnd;
        this.YEnd = YEnd;
    }

    public int getXBegin()
    {
        return XBegin;
    }

    public int getYBegin()
    {
        return YBegin;
    }

    public int getXEnd()
    {
        return XEnd;
    }

    public int getYEnd()
    {
        return YEnd;
    }

    public void drawCluster(BufferedImage image, Color color)
    {
        for (int i = XBegin; i <= XEnd; i++)
        {
            image.setRGB(i, YBegin, color.getRGB());
            image.setRGB(i, YEnd, color.getRGB());
        }
        for (int j = YBegin; j <= YEnd; j++)
        {
            image.setRGB(XBegin, j, color.getRGB());
            image.setRGB(XEnd, j, color.getRGB());
        }
    }

    public boolean isContainedBy(Cluster c)
    {
        return XBegin >= c.getXBegin() && XEnd <= c.getXEnd() && YBegin >= c.getYBegin() && YEnd <= c.getYEnd();
    }
}
