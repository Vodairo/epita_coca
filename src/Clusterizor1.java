import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Created by thomas on 20/06/15.
 */
public class Clusterizor1
{
    private static ArrayList<Cluster> clusters;
    private static BufferedImage image;

    public static void drawClusters(Color color)
    {
        for (Cluster c : clusters)
            c.drawCluster(image, color);
    }

    public static ArrayList<Cluster> clusterize(BufferedImage img, int step, int redCount)
    {
        image = img;
        clusters = new ArrayList<Cluster>();

        for (int i = 0; i < image.getWidth(); i++)
        {
            for (int j = 0; j < image.getHeight(); j++)
            {
                if (image.getRGB(i, j) == Color.gray.getRGB() && !inExistingCluster(i, j))
                {
                    Cluster c = getCluster(i, j, step, redCount);
                    int n = 0;
                    while (n < clusters.size() && !clusterCollision(clusters.get(n), c))
                        n++;

                    if (n < clusters.size())
                    {
                        Cluster prev_c = clusters.get(n);
                        clusters.remove(n);
                        clusters.add(n, joinCluster(prev_c, c));
                    }
                    else
                        clusters.add(c);
                }
            }
        }

        joinAllCluster();
        cleanClusters();

        return clusters;
    }

    private static Cluster getCluster(int x, int y, int step, int redCount)
    {
        int XBegin = x;
        int XEnd = x + step;
        int YBegin = y;
        int YEnd = y + step;

        while (inCLuster(x, y, step, redCount) && !inExistingCluster(x, y))
        {
            int j = y;
            while (inCLuster(x, j, step, redCount) && !inExistingCluster(x, j))
                j+= step;
            if (j > YEnd)
                YEnd = j;

            j = y;
            while (inCLuster(x, j, step, redCount) && !inExistingCluster(x, j))
                j -= step;
            if (j < YBegin)
                YBegin = j;

            x += step;
        }
        if (x > XEnd)
            XEnd = x;

        if (XBegin < 0)
            XBegin = 0;
        if (XEnd >= image.getWidth())
            XEnd = image.getWidth() - 1;
        if (YBegin < 0)
            YBegin = 0;
        if (YEnd >= image.getHeight())
            YEnd = image.getHeight() -1;
        return new Cluster(XBegin, YBegin, XEnd, YEnd);
    }

    private static boolean inCLuster(int x, int y, int step, int redCount)
    {
        int red = 0;
        for (int i = x; 0 <= i && i < image.getWidth() && i < x + step; i++)
        {
            for (int j = y; 0 <= j && j < image.getHeight() && j < y + step; j++)
            {
                if (image.getRGB(i, j) == Color.gray.getRGB())
                    red++;
            }
        }
        return red >= redCount;
    }

    private static boolean inExistingCluster(int x, int y)
    {
        for (Cluster c : clusters)
        {
            if (c.getXBegin() <= x && x <= c.getXEnd() && c.getYBegin() <= y && y <= c.getYEnd())
                return true;
        }
        return false;
    }

    private static boolean clusterCollision(Cluster c1, Cluster c2)
    {
        if (c1 == null || c2 == null)
            return false;

        boolean c1Inc2 = (c2.getXBegin() - 1 <= c1.getXBegin() && c1.getXBegin() <= c2.getXEnd() + 1 && c2.getYBegin() - 1 <= c1.getYBegin() && c1.getYBegin() <= c2.getYEnd() + 1)
                || (c2.getXBegin() - 1 <= c1.getXEnd() && c1.getXEnd() <= c2.getXEnd() + 1 && c2.getYBegin() - 1 <= c1.getYBegin() && c1.getYBegin() <= c2.getYEnd() + 1)
                || (c2.getXBegin() - 1 <= c1.getXBegin() && c1.getXBegin() <= c2.getXEnd() + 1 && c2.getYBegin() - 1 <= c1.getYBegin() && c1.getYBegin() <= c2.getYEnd() + 1)
                || (c2.getXBegin() - 1 <= c1.getXEnd() && c1.getXEnd() <= c2.getXEnd() + 1 && c2.getYBegin() - 1 <= c1.getYEnd() && c1.getYEnd() <= c2.getYEnd() + 1);
        boolean c2Inc1 = (c1.getXBegin() - 1 <= c2.getXBegin() && c2.getXBegin() <= c1.getXEnd() + 1 && c1.getYBegin() - 1 <= c2.getYBegin() && c2.getYBegin() <= c1.getYEnd() + 1)
                || (c1.getXBegin() - 1 <= c2.getXEnd() && c2.getXEnd() <= c1.getXEnd() + 1 && c1.getYBegin() - 1 <= c2.getYBegin() && c2.getYBegin() <= c1.getYEnd() + 1)
                || (c1.getXBegin() - 1 <= c2.getXBegin() && c2.getXBegin() <= c1.getXEnd() + 1 && c1.getYBegin() - 1 <= c2.getYBegin() && c2.getYBegin() <= c1.getYEnd() + 1)
                || (c1.getXBegin() - 1 <= c2.getXEnd() && c2.getXEnd() <= c1.getXEnd() + 1 && c1.getYBegin() - 1 <= c2.getYEnd() && c2.getYEnd() <= c1.getYEnd() + 1);

        return c1Inc2 || c2Inc1;
    }

    private static Cluster joinCluster(Cluster c1, Cluster c2)
    {
        int XBegin = Math.min(c1.getXBegin(), c2.getXBegin());
        int YBegin = Math.min(c1.getYBegin(), c2.getYBegin());
        int XEnd = Math.max(c1.getXEnd(), c2.getXEnd());
        int YEnd = Math.max(c1.getYEnd(), c2.getYEnd());

        return new Cluster(XBegin, YBegin, XEnd, YEnd);
    }

    private static void joinAllCluster()
    {
        for (int i = 0; i < clusters.size(); i++)
        {
            Cluster ci = clusters.get(i);
            for (int j = i + 1; j < clusters.size(); j++)
            {
                Cluster cj = clusters.get(j);
                if (clusterCollision(ci, cj))
                {
                    clusters.remove(j--);
                    clusters.remove(i);
                    clusters.add(i, joinCluster(ci, cj));
                }
            }
        }
    }

    private static void cleanClusters()
    {
        int i = 0;
        while (i < clusters.size())
        {
            Cluster c = clusters.get(i);
            if (c.getXEnd() - c.getXBegin() < 15 || c.getYEnd() - c.getYBegin() < 15)
                clusters.remove(i);
            else if (((double) (c.getXEnd() - c.getXBegin()) / (c.getYEnd() - c.getYBegin()) > 2
                    || (double) (c.getYEnd() - c.getYBegin()) / (c.getXEnd() - c.getXBegin()) > 2))
                clusters.remove(i);
            else if (getWhitePourcentage(c) < 0.1)
                clusters.remove(i);
            else
                i++;
        }
    }

    private static double getWhitePourcentage(Cluster c)
    {
        double white = 0;

        for (int i = c.getXBegin(); i <= c.getXEnd(); i++)
            for (int j = c.getYBegin(); j <= c.getYEnd(); j++)
                if (image.getRGB(i, j) == Color.white.getRGB())
                    white++;

        double size = (c.getXEnd() - c.getXBegin()) * (c.getYEnd() - c.getYBegin());
        return white / size;
    }
}
