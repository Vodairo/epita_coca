import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;

/**
 * Created by thomas on 20/06/15.
 */
public class Clusterizor2
{
    private static ArrayList<Cluster> clusters;
    private static BufferedImage image;

    public static void drawClusters(Color color)
    {
        for (Cluster c : clusters)
            c.drawCluster(image, color);
    }

    public static ArrayList<Cluster> clusterize(BufferedImage img)
    {
        clusters = new ArrayList<Cluster>();
        image = img;

        for (int i = 0; i < image.getWidth(); i++)
        {
            for (int j = 0; j < image.getHeight(); j++)
            {
                if (image.getRGB(i, j) == Color.gray.getRGB() && !inExistingCluster(i, j))
                {
                    Cluster c = getCluster(i, j);
                    int n = 0;
                    while (n < clusters.size() && !clusterCollision(clusters.get(n), c))
                        n++;

                    if (n < clusters.size())
                    {
                        Cluster prev_c = clusters.get(n);
                        clusters.remove(n);
                        clusters.add(n, joinCluster(prev_c, c));
                    }
                    else
                        clusters.add(c);
                }
            }
        }

        joinAllCluster();
        cleanClusters();

        return clusters;
    }

    private static Cluster getCluster(int x, int y)
    {
        int step = 1;

        int XBegin = x;
        int XEnd = x + step;
        int YBegin = y;
        int YEnd = y + step;

        while (inCLuster(x, y, step) && !inExistingCluster(x, y))
        {
            int j = y;
            while (inCLuster(x, j, step) && !inExistingCluster(x, y))
                j+= step;
            if (j > YEnd)
                YEnd = j;

            j = y;
            while (inCLuster(x, j, step) && !inExistingCluster(x, y))
                j -= step;
            if (j < YBegin)
                YBegin = j;

            x += step;
        }
        if (x > XEnd)
            XEnd = x;

        if (XBegin < 0)
            XBegin = 0;
        if (XEnd >= image.getWidth())
            XEnd = image.getWidth() - 1;
        if (YBegin < 0)
            YBegin = 0;
        if (YEnd >= image.getHeight())
            YEnd = image.getHeight() -1;
        return new Cluster(XBegin, YBegin, XEnd, YEnd);
    }

    private static boolean inCLuster(int x, int y, int step)
    {
        int red = 0;
        for (int i = x; 0 <= i && i < image.getWidth() && i < x + step; i++)
        {
            for (int j = y; 0 <= j && j < image.getHeight() && j < y + step; j++)
            {
                if (image.getRGB(i, j) == Color.gray.getRGB())
                    red++;
            }
        }
        return red >= 1;
    }

    private static boolean inExistingCluster(int x, int y)
    {
        for (Cluster c : clusters)
        {
            if (c.getXBegin() <= x && x <= c.getXEnd() && c.getYBegin() <= y && y <= c.getYEnd())
                return true;
        }
        return false;
    }

    private static boolean clusterCollision(Cluster c1, Cluster c2)
    {
        if (c1 == null || c2 == null)
            return false;

        boolean axisX = (c2.getYBegin() <= c1.getYBegin() && c1.getYBegin() <= c2.getYEnd())
                || (c2.getYBegin() <= c1.getYEnd() && c1.getYEnd() <= c2.getYEnd() || (c1.getYBegin() <= c2.getYBegin() && c2.getYEnd() <= c1.getYEnd()));;
        boolean axisY = (c2.getXBegin() <= c1.getXBegin() && c1.getXBegin() <= c2.getXEnd())
                || (c2.getXBegin() <= c1.getXEnd() && c1.getXEnd() <= c2.getXEnd() || (c1.getXBegin() <= c2.getXBegin() && c2.getXEnd() <= c1.getXEnd()));

        boolean right = c1.getXBegin() == c2.getXEnd() + 1 && axisX;
        boolean left = c1.getXEnd() == c2.getXBegin() - 1 && axisX;
        boolean top = c1.getYEnd() == c2.getYBegin() - 1 && axisY;
        boolean down = c1.getYBegin() == c2.getYEnd() + 1 && axisY;

        return right || left || top || down;
    }

    private static Cluster joinCluster(Cluster c1, Cluster c2)
    {
        int XBegin = Math.min(c1.getXBegin(), c2.getXBegin());
        int YBegin = Math.min(c1.getYBegin(), c2.getYBegin());
        int XEnd = Math.max(c1.getXEnd(), c2.getXEnd());
        int YEnd = Math.max(c1.getYEnd(), c2.getYEnd());

        return new Cluster(XBegin, YBegin, XEnd, YEnd);
    }

    private static void joinAllCluster()
    {
        for (int i = 0; i < clusters.size(); i++)
        {
            Cluster ci = clusters.get(i);
            int j = i + 1;
            while (j < clusters.size())
            {
                Cluster cj = clusters.get(j);
                if (cj.isContainedBy(ci) || ci.isContainedBy(cj))
                {
                    clusters.remove(j);
                    clusters.remove(i);
                    clusters.add(i, joinCluster(ci, cj));
                }
                else
                    j++;
            }
        }
    }

    private static void cleanClusters()
    {
        int i = 0;
        while (i < clusters.size())
        {
            Cluster c = clusters.get(i);
            if (c.getXEnd() - c.getXBegin() < 15 || c.getYEnd() - c.getYBegin() < 15)
                clusters.remove(i);
            else if (((double) (c.getXEnd() - c.getXBegin()) / (c.getYEnd() - c.getYBegin()) > 2
                    || (double) (c.getYEnd() - c.getYBegin()) / (c.getXEnd() - c.getXBegin()) > 2))
                clusters.remove(i);
            else if (getWhitePourcentage(c) < 0.1)
                clusters.remove(i);
            else
                i++;
        }
    }

    private static double getWhitePourcentage(Cluster c)
    {
        double white = 0;

        for (int i = c.getXBegin(); i <= c.getXEnd(); i++)
            for (int j = c.getYBegin(); j <= c.getYEnd(); j++)
                if (image.getRGB(i, j) == Color.white.getRGB())
                    white++;

        double size = (c.getXEnd() - c.getXBegin()) * (c.getYEnd() - c.getYBegin());
        return white / size;
    }
}
