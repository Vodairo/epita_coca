import java.awt.*;

/**
 * Created by thomas on 15/06/15.
 */
public class Convertor
{
    public static double[] pixelRgbToHsl(Color color)
    {
        double[] result = new double[3];

        double r = color.getRed() / 255.0;
        double g = color.getGreen() / 255.0;
        double b = color.getBlue() / 255.0;
        double cmax = Math.max(r, Math.max(g, b));
        double cmin = Math.min(r, Math.min(g, b));

        double d = cmax - cmin;
        double l = (cmax + cmin) / 2;
        double s = 0;
        double h = 60;

        if (d != 0)
            s = d / (1 - Math.abs(2 * l - 1));
        if (d == 0)
            h = 0;
        else if (cmax == r)
            h *= ((g - b) / d) % 6;
        else if (cmax == g)
            h *= ((b - r) / d) + 2;
        else
            h *= ((r - g) / d) + 4;

        result[0] = h;
        result[1] = s * 100;
        result[2] = l * 100;

        return result;
    }

    public static double[] pixelRgbToHsl(int r, int g, int b)
    {
        Color color = new Color(r, g, b);
        return pixelRgbToHsl(color);
    }
}
