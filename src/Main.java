import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * Created by thomas on 15/06/15.
 */
public class Main
{
    public static void main(String[] args)
    {
        if (args.length == 2)
        {
            try
            {
                Processor processor = new Processor(args[1]);

                File resultsDir = new File("results");
                resultsDir.mkdir();
                File mayBeDir = new File(args[0]);

                if (mayBeDir.isDirectory())
                {
                    File[] files = mayBeDir.listFiles();
                    if (files != null)
                    {
                        for (File file : files)
                        {
                            BufferedImage image = ImageIO.read(file);
                            BufferedImage origImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
                            for (int i = 0; i < image.getWidth(); i++)
                                for (int j = 0; j < image.getHeight(); j++)
                                    origImage.setRGB(i, j, image.getRGB(i, j));

                            Preprocessor preprocessor = new Preprocessor(image);
                            preprocessor.process();

                            ImageIO.write(processor.Process(preprocessor, origImage), "png", new File("results/" + file.getName()));
                        }
                    }
                }
                else
                {
                    BufferedImage image = ImageIO.read(mayBeDir);
                    BufferedImage origImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
                    for (int i = 0; i < image.getWidth(); i++)
                        for (int j = 0; j < image.getHeight(); j++)
                            origImage.setRGB(i, j, image.getRGB(i, j));

                    Preprocessor preprocessor = new Preprocessor(image);
                    image = preprocessor.process();

                    ImageIO.write(image, "png", new File("results/working_image.png"));
                    ImageIO.write(processor.Process(preprocessor, origImage), "png", new File("results/" + mayBeDir.getName()));
                }


            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else
            System.out.println("Usage : CocaDetector.jar (image) (models directory)");
    }
}
