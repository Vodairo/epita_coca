/**
 * Created by thomas on 20/06/15.
 */
public class Pair<F, S>
{
    public F FFirst;
    public S FSecond;

    public Pair(F first, S second)
    {
        FFirst = first;
        FSecond = second;
    }
}
