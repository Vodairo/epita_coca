import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Created by thomas on 15/06/15.
 */
public class Preprocessor
{
    private BufferedImage image;
    private ArrayList<Cluster> clusters;

    private int step = 2;

    //============
    // Constructor
    //============

    public Preprocessor(BufferedImage image)
    {
        this.image = image;

        clusters = new ArrayList<Cluster>();
    }

    //============
    // Getter / Setter
    //============

    public BufferedImage getImage()
    {
        return image;
    }

    public ArrayList<Cluster> getClusters()
    {
        return clusters;
    }

    //============
    // Other methods
    //============

    public BufferedImage process()
    {
        extractRedWhite();
        image = medianFilter();

        clusters.addAll(Clusterizor1.clusterize(image, step, 3));
        clusters.addAll(Clusterizor2.clusterize(image));
        removeDuplicateClusters();

        return image;
    }

    private void extractRedWhite()
    {
        for (int i = 0; i < image.getWidth(); i++)
        {
            for (int j = 0; j < image.getHeight(); j++)
            {
                Color c = new Color(image.getRGB(i, j));
                double[] hslPixel = Convertor.pixelRgbToHsl(c);
                if (c != Color.black && c.getRed() >= Math.max(c.getGreen(), c.getBlue()) * 2 && Math.abs(c.getGreen() - c.getBlue()) < 30)
                    image.setRGB(i, j, Color.gray.getRGB());
                else
                {
                    int moy = (c.getRed() + c.getGreen() + c.getBlue()) / 3;
                    int gray = (int) (0.299 * c.getRed() + 0.587 * c.getGreen() + 0.114 * c.getBlue());
                    int s = 40;
                    if ((Math.abs(moy - c.getRed()) < s && Math.abs(moy - c.getGreen()) < s && Math.abs(moy - c.getBlue()) < s && gray >= 128)
                            || (hslPixel[2] >= 55 && hslPixel[1] <= 40))
                        image.setRGB(i, j, Color.white.getRGB());
                    else
                        image.setRGB(i, j, Color.black.getRGB());
                }
            }
        }
    }

    private BufferedImage medianFilter()
    {
        BufferedImage img = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());

        int size = 1;
        int i = size;
        while (i < image.getWidth() - size)
        {
            int j = size;
            while (j < image.getHeight() - size)
            {
                ArrayList<Integer> neighbours = new ArrayList<Integer>();
                int x = -size;
                while (x <= size)
                {
                    int y = -size;
                    while (y <= size)
                    {
                        int color = image.getRGB(i + x, j + y);
                        int n = 0;
                        while (n < neighbours.size() && color > neighbours.get(n))
                            n++;
                        neighbours.add(n, color);
                        y++;
                    }
                    x++;
                }
                img.setRGB(i, j, neighbours.get(5));
                j++;
            }
            i++;
        }

        return img;
    }

    private void removeDuplicateClusters()
    {
        for (int i = 0; i < clusters.size(); i++)
        {
            Cluster c1 = clusters.get(i);

            int j = i + 1;
            while (j < clusters.size())
            {
                Cluster c2 = clusters.get(j);
                int diff = Math.abs(c1.getXBegin() - c2.getXBegin())
                        + Math.abs(c1.getYBegin() - c2.getYBegin())
                        + Math.abs(c1.getXEnd() - c2.getXEnd())
                        + Math.abs(c1.getYEnd() - c2.getYEnd());
                if (diff <= 16 * step)
                    clusters.remove(j);
                else
                    j++;
            }
        }
    }
}
