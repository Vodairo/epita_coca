import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by Voda on 16/06/2015.
 */
public class Processor {

    private BufferedImage FModel;

    private final int FFWidth = 152;
    private final int FFHeight = 256;

    private final float FFRatioHeightWidth = (float)FFHeight / FFWidth;

    private final double FFRatioWhiteRed = 0.2;
    private final double FFRatioToValidate = 0.75;

    public Processor(String path) throws Exception {
        Load(path);
    }

    public void Load(String path) throws Exception{

        File directory = new File(path);

        ArrayList<BufferedImage> images = new ArrayList<BufferedImage>();

        if (directory.exists()) {
            File[] files = directory.listFiles();
            if (files != null) {
                for (File file : files) {
                    images.add(ImageIO.read(file));
                }
            }
        }

        for (int i = 0; i < images.size(); i++) {
            Preprocessor tmp = new Preprocessor(images.get(i));
            images.set(i, ResizeToModel(tmp.process()));
        }

        FModel = new BufferedImage(FFWidth, FFHeight, BufferedImage.TYPE_INT_RGB);

        for (int x = 0; x < FFWidth; x++) {
            for (int y = 0; y < FFHeight; y++) {
                int red = 0;
                int green = 0;
                int blue = 0;

                for (BufferedImage image : images) {
                    Color color = new Color(image.getRGB(x, y));
                    red += color.getRed();
                    green += color.getGreen();
                    blue += color.getBlue();
                }

                FModel.setRGB(x, y, new Color(red / images.size(), green / images.size(), blue / images.size()).getRGB());
            }
        }
    }

    public BufferedImage Process(Preprocessor preprocessed, BufferedImage origImage) throws Exception {

        int clusterFound = 0;

        for (Cluster cluster : preprocessed.getClusters()) {
            BufferedImage tmp = preprocessed.getImage().getSubimage(cluster.getXBegin(), cluster.getYBegin(), cluster.getXEnd() - cluster.getXBegin(), cluster.getYEnd() - cluster.getYBegin());
            float ratioHeightWidth = (float)tmp.getHeight() / tmp.getWidth();
            if (ratioHeightWidth < FFRatioHeightWidth / 2) {
                double angle;
                if (tmp.getHeight() == tmp.getWidth()) {
                    angle = 45;
                } else {
                    angle = ((double)(tmp.getHeight() * FFWidth - tmp.getWidth() * FFHeight) / (double)(tmp.getWidth() * FFHeight)) / ((double)(FFWidth * FFWidth - FFHeight * FFHeight) / (double)(FFHeight * FFHeight)) * 90;
                }
                double[] ratio1 = Compare(ResizeToModel(rotate(tmp, angle)));
                double[] ratio2 = Compare(ResizeToModel(rotate(tmp, -angle)));
                System.out.println(ratio1[0] + "(>" + FFRatioToValidate + ") & " + ratio1[1] + "(<" + FFRatioWhiteRed + ") or " + ratio2[0] + "(>" + FFRatioToValidate + ") & " + ratio2[1] + "(<" + FFRatioWhiteRed + ")" + "(angle: " + angle + ")");
                if (ratio1[0] > FFRatioToValidate && ratio1[1] < FFRatioWhiteRed || ratio2[0] > FFRatioToValidate && ratio2[1] < FFRatioWhiteRed) {
                    cluster.drawCluster(origImage, Color.green);
                    clusterFound += 1;
                }
            } else {
                double[] ratio = Compare(ResizeToModel(tmp));
                System.out.println(ratio[0] + "(>" + FFRatioToValidate + ") & " + ratio[1] + "(<" + FFRatioWhiteRed + ")");
                if (ratio[0] > FFRatioToValidate && ratio[1] < FFRatioWhiteRed) {
                    cluster.drawCluster(origImage, Color.green);
                    clusterFound += 1;
                }
            }

        }

        System.out.println("Number of cans found : " + clusterFound);

        return origImage;
    }

    private BufferedImage ResizeToModel(BufferedImage image) {
        BufferedImage scaledBI = new BufferedImage(FFWidth, FFHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = scaledBI.createGraphics();
        g.setComposite(AlphaComposite.Src);
        g.drawImage(image, 0, 0, FFWidth, FFHeight, null);
        g.dispose();
        return scaledBI;
    }

    public static BufferedImage rotate(Image img, double angle)
    {
        double sin = Math.abs(Math.sin(Math.toRadians(angle))),
               cos = Math.abs(Math.cos(Math.toRadians(angle)));

        int w = img.getWidth(null), h = img.getHeight(null);

        int neww = (int) Math.floor(w*cos + h*sin),
            newh = (int) Math.floor(h*cos + w*sin);

        BufferedImage bimg = toBufferedImage(new BufferedImage(neww, newh, BufferedImage.TYPE_INT_RGB));
        Graphics2D g = bimg.createGraphics();

        g.translate((neww-w)/2, (newh-h)/2);
        g.rotate(Math.toRadians(angle), w/2, h/2);
        g.drawRenderedImage(toBufferedImage(img), null);
        g.dispose();

        return bimg;
    }

    public static BufferedImage toBufferedImage(Image img){
        if (img instanceof BufferedImage) {
            return (BufferedImage) img;
        }
        // Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();
        // Return the buffered image
        return bimage;
    }

    private double[] Compare(BufferedImage image) {
        double similarities = 0;

        long white = 0;
        long red = 0;

        for (int x = 0; x < FModel.getWidth(); x++) {
            for (int y = 0; y < FModel.getHeight(); y++) {
                Color modelColor = new Color(FModel.getRGB(x, y));
                Color imageColor = new Color(image.getRGB(x, y));

                int diff = Math.abs(imageColor.getRed() - modelColor.getRed());
                if (diff < 92.0f) {
                    similarities += 1;
                    if (imageColor.getRed() == 128)
                        red += 1;
                    else if (imageColor.getRed() == 0)
                        white += 1;
                } else if (diff < 138.0f) {
                    similarities += 0.5;
                }
            }
        }

        double[] result = new double[2];
        result[0] = similarities / (FModel.getHeight() * FModel.getWidth());
        result[1] = (double)white / red;

        return result;
    }
}
